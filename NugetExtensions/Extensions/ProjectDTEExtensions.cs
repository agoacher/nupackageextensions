﻿using EnvDTE;
using System.Collections.Generic;

namespace Nu.Extensions
{
    public static class ProjectDTEExtensions
    {
        public static IReadOnlyList<Project> ToReadOnlyList(this Projects projects)
        {
            var list = new List<Project>();

            if (projects == null)
            {
                return list;
            }

            foreach (var project in projects)
            {
                if(project is Project p)
                {
                    list.Add(p);
                }
            }

            return list;
        }
    }
}
