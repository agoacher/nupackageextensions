﻿namespace Nu.Pack
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public sealed class PackInfo
    {
        public string Directory { get; set; }

        public string Name { get; set; }

        public string FullName { get; set; }

        public string OutputPath { get; set; }

        public string BuildName { get; set; }
    }
}
