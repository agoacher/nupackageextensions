﻿namespace Nu.Pack
{
    using Nu.Pack.Manifest;
    using NuGet.Packaging;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    internal class NuGetPackageBuilder
    {
        private readonly PackInfo project;
        private readonly Version nugetVersion;
        private readonly List<string> errors;
        private readonly NugetConfig config;
        private readonly string outputFolder;

        private static readonly string NuSpecExtension = ".nuspec";

        /// <summary>
        /// Lifted from: https://github.com/NuGet/NuGet.Client/blob/9fe40c06ab14f77848dd8ebe3a8e5cdc912cf0e3/src/NuGet.Core/NuGet.Commands/CommandRunners/PackCommandRunner.cs
        /// These are the allowed extensions for trying to load manifest information
        /// </summary>
        private static readonly HashSet<string> nuget_allowedExtensions = new HashSet<string>
        {
           NuSpecExtension,
            ".csproj",
            ".vbproj",
            ".fsproj",
            ".nproj",
            ".btproj",
            ".dxjsproj",
            ".json",
        };

        public NuGetPackageBuilder(PackInfo project, Version nugetVersion, NugetConfig config, string outputFolder)
        {
            this.project = project;
            this.nugetVersion = nugetVersion;
            errors = new List<string>();
            this.config = config;
            this.outputFolder = outputFolder;
        }

        public IEnumerable<string> Errors => errors;

        public bool Build()
        {
            errors.Clear();
            NuGet.Packaging.Manifest manifest = GetManifest(project.Directory);
            if (manifest == null)
            {
                return false;
            }

            CreateNuGetPackageFromManifest(config, manifest, outputFolder);
            return true;
        }

        private NuGet.Packaging.Manifest GetManifest(string directory)
        {
            var files = Directory.GetFiles(directory);
            var allowedFiles = GetAllowedFiles(files);
            Dictionary<string, NuGet.Packaging.Manifest> manifests = GetManifestFiles(allowedFiles, config);

            manifests.TryGetValue(".nuspec", out NuGet.Packaging.Manifest nuspecManifest);
            foreach (KeyValuePair<string, NuGet.Packaging.Manifest> kvp in manifests)
            {
                if (kvp.Key.Equals(".nuspec", StringComparison.InvariantCultureIgnoreCase))
                {
                    continue;
                }

                if (nuspecManifest == null)
                {
                    nuspecManifest = kvp.Value;
                }
                else
                {
                    ICollection<ManifestFile> mergingFiles = kvp.Value.Files;

                    foreach (ManifestFile mergingFile in mergingFiles)
                    {
                        if (!nuspecManifest.Files.Any(f => f.Source.Equals(mergingFile.Source, StringComparison.InvariantCultureIgnoreCase)))
                        {
                            nuspecManifest.Files.Add(mergingFile);
                        }
                    }
                }
            }

            return nuspecManifest ;
        }

        private static Dictionary<string, NuGet.Packaging.Manifest> GetManifestFiles(string[] files, NugetConfig config)
        {
            var manifestFiles = new Dictionary<string, NuGet.Packaging.Manifest>();
            foreach (var file in files)
            {
                IManifestBuilder builder = GetBuilder(file, config);
                NuGet.Packaging.Manifest manifest = builder.Create();
                if (manifest != null)
                {
                    manifestFiles.Add(Path.GetExtension(file), manifest);
                }
            }
            return manifestFiles;
            //var manifestFiles = new List<NuGet.Packaging.Manifest>();

            //foreach (var file in files)
            //{
            //    try
            //    {
            //        using (var stream = File.OpenRead(file))
            //        {
            //            var manifestFile = NuGet.Packaging.Manifest.ReadFrom(stream, false);
            //            manifestFiles.Add(manifestFile);
            //        }
            //    }
            //    catch (InvalidOperationException ioe)
            //    {
            //        var root = Path.GetDirectoryName(file);
            //        var packageConfig = Path.Combine(root, "package.config");
            //        if (File.Exists(packageConfig))
            //        {
            //            using (var stream = File.OpenRead(packageConfig))
            //            {
            //                var manifestFile = NuGet.Packaging.Manifest.ReadFrom(stream, false);
            //                manifestFiles.Add(manifestFile);
            //            }
            //        }
            //    }
            //    catch(Exception e)
            //    {

            //        System.Diagnostics.Debugger.Break();
            //    }
            //}
            //return manifestFiles.ToArray();
        }

        private static IManifestBuilder GetBuilder(string file, NugetConfig config)
        {
            var extension = Path.GetExtension(file);
            switch (extension)
            {
                case ".nuspec": return new NuSpecManifestBuilder(file);
                case ".json": return new JsonManifestBuilder(file);
                case ".config": return new PackageConfigManifestBuilder(file);
                default:
                    {
                        return GetProjectManifestBuilder(file, config);
                    }
            }
        }

        private static IManifestBuilder GetProjectManifestBuilder(string file, NugetConfig config)
        {
            using (FileStream stream = File.OpenRead(file))
            {
                var root = XDocument.Load(stream);
                XElement project = (from r in root.Descendants("Project") select r).SingleOrDefault();
                if (project != null)
                {
                    if (project.Attributes("Sdk").Any())
                    {
                        return new PackageReferenceManifestBuilder(file, config);
                    }
                }

                return new ProjectManifestBuilder(config, file);
            }

            throw new InvalidOperationException("No builder exists for this project file.");
        }

        private static string[] GetAllowedFiles(string[] filePaths)
        {
            return filePaths.Where(fp =>
            {
                var extension = Path.GetExtension(fp);
                return nuget_allowedExtensions.Contains(extension);
            }).ToArray();
        }

        public static void CreateNuGetPackageFromManifest(NugetConfig cfg, NuGet.Packaging.Manifest manifest, string outputFolder)
        {
            if (!Directory.Exists(outputFolder))
            {
                Directory.CreateDirectory(outputFolder);
            }

            BuildPackage(cfg, manifest.Metadata, outputFolder,
                $"{cfg.ProjectName}-{cfg.GetVersionString()}.nupkg",
                manifest.Files.ToArray());

            if (!cfg.CreateSymbols)
            {
                return;
            }

            var files = new List<ManifestFile>();

            foreach (ManifestFile file in manifest.Files)
            {
                var source = file.Source;
                var extension = Path.GetExtension(source);
                var pdbSource = Path.ChangeExtension(source, ".pdb");

                if (!files.Any(f => f.Source.Equals(pdbSource, System.StringComparison.InvariantCultureIgnoreCase)))
                {
                    if (Path.GetExtension(source).Equals(".dll", System.StringComparison.InvariantCultureIgnoreCase))
                    {
                        var pdbFile = new ManifestFile
                        {
                            Source = Path.ChangeExtension(source, ".pdb"),
                            Target = Path.ChangeExtension(file.Target, "pdb")
                        };

                        files.Add(pdbFile);
                    }
                }

                files.Add(file);
            }

            BuildPackage(cfg, manifest.Metadata, outputFolder,
                 $"{cfg.ProjectName}-{cfg.GetVersionString()}.symbols.nupkg",
                 files.ToArray());

        }

        private static void BuildPackage(NugetConfig cfg, ManifestMetadata metaData, string outputFolder, string packageName, ManifestFile[] files)
        {
            var packageBuilder = new PackageBuilder();
            packageBuilder.PopulateFiles(cfg.ProjectRoot, files);
            packageBuilder.Populate(metaData);
            var outputPath = Path.Combine(outputFolder, packageName);
            using (FileStream stream = File.Open(outputPath, FileMode.Create))
            {
                packageBuilder.Save(stream);
            }
        }
    }
}
