﻿namespace Nu.Pack
{
    using EnvDTE;
    using IniParser;
    using IniParser.Model;
    using Microsoft.VisualStudio.Shell;
    using Nu.Commands;
    using Nu.Config;
    using Nu.Extensions;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Design;
    using System.IO;
    using System.Threading.Tasks;

    public sealed class PackSolutionCommand : CommandBase<PackSolutionCommand>
    {
        public PackSolutionCommand() : base(
            new CommandID(CommandConstants.CommandSet,
                CommandConstants.PackageLocalNugetCommandId))
        {
        }

        public override void Execute(Package package, DTE dte)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            var projectsList = dte.Solution.Projects as Projects;
            if (projectsList == null)
            {
                return;
            }

            IReadOnlyList<Project> projects = projectsList.ToReadOnlyList();
            IReadOnlyList<PackInfo> packInfo = GetPackInfo(projects);

            Version v = Settings.GetDefaultVersion();

            foreach (PackInfo project in packInfo)
            {
                var target = project.Name;
                try
                {
                    OutputWriter.WriteLine($"Attempting packaging of {project.Name}");
                    var versionFile = Path.Combine(Settings.TargetDirectory, $"{project.Name}.version");
                    Version nugetVersion = LoadVersion(versionFile, v);

                    var cfg = NugetConfig.Config()
                            .ForProject(project.BuildName)
                            .ThatsLocatedAt(project.Directory)
                            .OnVersion(nugetVersion)
                            .WithBinariesAt(project.OutputPath)
                            .WithSymbols(Settings.GenerateSymbols)
                            .Build();

                    var nugetBuilder = new NuGetPackageBuilder(project, nugetVersion, cfg, Settings.TargetDirectory);
                    if (nugetBuilder.Build())
                    {
                        SaveVersion(versionFile, nugetVersion);
                        OutputWriter.WriteLine("Done");
                    }
                    else
                    {
                        foreach (var error in nugetBuilder.Errors)
                        {
                            OutputWriter.WriteLine(error);
                        }
                    }

    

                    //target = Path.Combine(project.Directory, $"{project.Name}.nuspec");
                    //var nuspecManifestReader = new NuspecManifestReader(target);
                    //var manifest = nuspecManifestReader.GetManifest();
                    //if (manifest == null)
                    //{
                    //    target = project.FullName;

                    //    NuGetPackageBuilder.CreateNuGetPackage(cfg, Settings.TargetDirectory);
                    //}
                    //else
                    //{
                    //    NuGetPackageBuilder.CreateNuGetPackageFromManifest(cfg, manifest, Settings.TargetDirectory);
                    //}

                }
                catch (Exception e)
                {
                    OutputWriter.WriteLine($"Failed to package: {target}");
                    OutputWriter.WriteLine(e.Message);
                    OutputWriter.WriteLine("");
                }

                OutputWriter.WriteLine();
            }
        }

        protected override async Task<PackSolutionCommand> OnInitializeAsync(AsyncPackage package)
        {
            return new PackSolutionCommand();
        }

        private Version LoadVersion(string versionFile, Version defaultVersion)
        {
            var parser = new FileIniDataParser();
            if (!File.Exists(versionFile))
            {
                return defaultVersion;
            }

            IniData data = parser.ReadFile(versionFile);
            KeyDataCollection versionData = data["Version"];
            var current = versionData["current"];
            if (Version.TryParse(current, out Version result))
            {
                return new Version(result.Major, result.Minor, result.Build + 1);
            }
            return defaultVersion;
        }

        private void SaveVersion(string versionFile, Version version)
        {
            var parser = new FileIniDataParser();
            IniData data = null;
            if (!File.Exists(versionFile))
            {
                data = new IniParser.Model.IniData();
            }
            else
            {
                data = parser.ReadFile(versionFile);
            }

            KeyDataCollection versionSection = data["Version"];
            versionSection["current"] = version.ToString();
            parser.WriteFile(versionFile, data);
        }

        private IReadOnlyList<PackInfo> GetPackInfo(IReadOnlyList<Project> projects)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            var list = new List<PackInfo>();

            foreach (Project project in projects)
            {
                try
                {
                    list.Add(new PackInfo
                    {
                        Directory = Path.GetDirectoryName(project.FullName),
                        Name = project.Name,
                        FullName = project.FullName,
                        OutputPath = project.ConfigurationManager.ActiveConfiguration
                            .Properties.Item("OutputPath").Value.ToString(),
                        BuildName = project.Properties.Item("AssemblyName").Value.ToString()
                    });
                }
                catch (Exception e)
                {
                    OutputWriter.WriteLine($"Ignoring project: {project.Name}");
                    OutputWriter.WriteLine(e.Message);
                    OutputWriter.WriteLine();
                }
            }

            return list;
        }
    }
}
