﻿namespace Nu.Pack
{
    using NuGet.Packaging;
    using NuGet.Versioning;
    using System;
    using System.IO;

    public sealed partial class NugetConfig
    {
        private readonly string version;
        private readonly string projectName;
        private readonly string binFolder;

        private NugetConfig(bool generateSymbols, Version version,
            string projectName, string projectRoot, string binFolder)
        {
            CreateSymbols = generateSymbols;
            this.version = $"{version.ToString()}-local";
            this.projectName = projectName;
            ProjectRoot = projectRoot;
            this.binFolder = binFolder;
        }

        public bool CreateSymbols { get; }

        public string ProjectRoot { get; }

        public string ProjectName => projectName;

        public string GetVersionString() { return version; }

        public ManifestMetadata GetManifest()
        {
            return new ManifestMetadata
            {
                Id = projectName,
                Version = GetNuGetVersion(),
                Authors = new[] { "Nu Local" },
                Description = "Local development build"
            };
        }

        public ManifestFile GetManifestFile(string extension)
        {
            var source = Path.Combine(ProjectRoot, binFolder, projectName + extension);
            if(!File.Exists(source))
            {
                return null;
            }

            return new ManifestFile
            {
                Source = source,
                Target = @"lib\" + projectName + extension
            };
        }

        private NuGetVersion GetNuGetVersion()
        {
            return new NuGetVersion(GetVersionString());
        }
    }

}
