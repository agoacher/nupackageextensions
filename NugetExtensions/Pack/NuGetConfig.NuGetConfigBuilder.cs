﻿namespace Nu.Pack
{
    using System;

    public sealed partial class NugetConfig
    {
        public static NuGetConfigBuilder Config()
        {
            return new NuGetConfigBuilder();
        }

        public sealed class NuGetConfigBuilder
        {
            private string projectName = null;
            private string projectRoot = null;
            private string binFolder = null;
            private Version version = null;
            private bool generateSymbols = true;

            public NuGetConfigBuilder ForProject(string name)
            {
                if (string.IsNullOrEmpty(name))
                {
                    throw new NullReferenceException(nameof(name));
                }

                projectName = name;
                return this;
            }

            public NuGetConfigBuilder ThatsLocatedAt(string projectRoot)
            {
                if (string.IsNullOrEmpty(projectRoot))
                {
                    throw new NullReferenceException(nameof(projectRoot));
                }

                this.projectRoot = projectRoot;
                return this;
            }

            public NuGetConfigBuilder WithBinariesAt(string binFolder)
            {
                if (string.IsNullOrEmpty(binFolder))
                {
                    throw new NullReferenceException(nameof(binFolder));
                }

                this.binFolder = binFolder;
                return this;
            }

            public NuGetConfigBuilder OnVersion(Version version)
            {
                this.version = version ?? throw new NullReferenceException(nameof(version));
                return this;
            }

            public NuGetConfigBuilder WithSymbols(bool generateSymbols)
            {
                this.generateSymbols = generateSymbols;
                return this;
            }

            public NugetConfig Build()
            {
                if (string.IsNullOrEmpty(projectName))
                {
                    throw new InvalidOperationException("Cannot create without a valid project");
                }

                if (string.IsNullOrEmpty(projectRoot))
                {
                    throw new InvalidOperationException("Cannot create without knowing where the project sits");
                }

                if (string.IsNullOrEmpty(binFolder))
                {
                    binFolder = "\\bin\\Debug";
                }

                if (version == null)
                {
                    throw new InvalidOperationException("Need to know what version of the package to build");
                }

                return new NugetConfig(generateSymbols, version, projectName, projectRoot, binFolder);
            }
        }
    }
}
