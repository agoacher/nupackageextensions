﻿namespace Nu.Pack.Manifest
{
    using NuGet.Packaging;
    using System.Collections.Generic;

    public interface IManifestBuilder
    {
        Manifest Create();
        IEnumerable<string> Errors { get; }
    }
}
