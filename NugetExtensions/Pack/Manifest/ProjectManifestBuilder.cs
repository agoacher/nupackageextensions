﻿namespace Nu.Pack.Manifest
{
    using NuGet.Packaging;
    using System.Collections.Generic;

    public sealed class ProjectManifestBuilder : IManifestBuilder
    {
        private readonly string file;
        private readonly NugetConfig config;
        private readonly List<string> errors = new List<string>();

        public ProjectManifestBuilder(NugetConfig cfg, string file)
        {
            this.file = file;
            config = cfg;
        }

        public IEnumerable<string> Errors => errors;

        public Manifest Create()
        {
            errors.Clear();

            var manifestData = config.GetManifest();
            var manifest = new Manifest(manifestData);

            var dll = config.GetManifestFile(".dll");
            var pdb = config.GetManifestFile(".pdb)");

            manifest.Files.Add(dll);
            if (config.CreateSymbols && pdb != null)
            {
                manifest.Files.Add(pdb);
            }

            return manifest;
        }
    }
}
