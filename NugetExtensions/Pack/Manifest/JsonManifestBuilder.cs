﻿namespace Nu.Pack.Manifest
{
    using NuGet.Packaging;
    using System;
    using System.Collections.Generic;

    public sealed class JsonManifestBuilder : IManifestBuilder
    {
        private readonly string file;
        private readonly List<string> errors = new List<string>();

        public JsonManifestBuilder(string file)
        {
            this.file = file;
        }

        public IEnumerable<string> Errors => errors;

        public Manifest Create()
        {
            errors.Add("Json files are unsupported");
            return null;
        }
    }
}
