﻿namespace Nu.Pack.Manifest
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using NuGet.Packaging;

    public sealed class PackageConfigManifestBuilder : IManifestBuilder
    {
        private readonly string file;
        private readonly List<string> errors = new List<string>();

        public PackageConfigManifestBuilder(string file)
        {
            this.file = file;
        }

        public IEnumerable<string> Errors => errors;

        public Manifest Create()
        {
            errors.Clear();
            var fileName = Path.GetFileName(file);
            if(!fileName.Equals("package.config", StringComparison.InvariantCultureIgnoreCase))
            {
                return null;
            }

            try
            {
                using (var stream = File.OpenRead(file))
                {
                    return Manifest.ReadFrom(stream, true);
                }
            }
            catch (Exception e)
            {
                errors.Add(e.Message);
                return null;
            }
        }
    }
}
