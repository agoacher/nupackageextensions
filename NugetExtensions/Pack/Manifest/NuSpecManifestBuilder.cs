﻿namespace Nu.Pack.Manifest
{
    using NuGet.Packaging;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public sealed class NuSpecManifestBuilder : IManifestBuilder
    {
        private readonly string file;
        private readonly List<string> errors = new List<string>();

        public NuSpecManifestBuilder(string file)
        {
            this.file = file;
        }

        public IEnumerable<string> Errors => errors;

        public Manifest Create()
        {
            errors.Clear();
            try
            {
                using (var nuspecFileStream = File.OpenRead(file))
                {
                    return NuGet.Packaging.Manifest.ReadFrom(nuspecFileStream, true);
                }
            }
            catch (InvalidOperationException ioe)
            {
                // todo: Handle specific case where replacement variables still in nuspec.
                errors.Add(ioe.Message);
            }
            catch(Exception e)
            {
                errors.Add(e.Message);
            }

            return null;
        }
    }
}
