﻿namespace Nu.Pack.Manifest
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using NuGet.Packaging;
    using NuGet.Packaging.Core;

    public sealed class PackageReferenceManifestBuilder : IManifestBuilder
    {
        private readonly string file;
        private readonly NugetConfig config;
        private readonly List<string> errors = new List<string>();

        public PackageReferenceManifestBuilder(string file, NugetConfig cfg)
        {
            this.file = file;
            config = cfg;
        }

        public IEnumerable<string> Errors => errors;

        public Manifest Create()
        {
            errors.Clear();

            try
            {
                using (var stream = File.OpenRead(file))
                {
                    var document = XDocument.Load(stream);
                    var project = (from d in document.Descendants("Project") select d).SingleOrDefault();
                    if(project == null)
                    {
                        errors.Add("Project node is missing!");
                        return null;
                    }

                    var packageRefs = from p in project.Descendants("ItemGroup").Descendants("PackageReference") select p;
                    var manifestData = config.GetManifest();
                    var framework = NuGet.Frameworks.NuGetFramework.AnyFramework;
                    var packages = new List<PackageDependency>();

                    foreach (var package in packageRefs)
                    {
                        var name = package.Attribute("Include").Value;
                        var versionStr = package.Attribute("Version").Value;

                        // todo: Handle this better
                        packages.Add(new PackageDependency(name, new NuGet.Versioning.VersionRange(new NuGet.Versioning.NuGetVersion(versionStr))));
                    }

                    var group = new PackageDependencyGroup(framework, packages);
                    manifestData.DependencyGroups = new[] { group };

                    var manifest = new Manifest(manifestData);

                    var dll = config.GetManifestFile(".dll");
                    var pdb = config.GetManifestFile(".pdb)");

                    manifest.Files.Add(dll);
                    if(config.CreateSymbols && pdb != null)
                    {
                        manifest.Files.Add(pdb);
                    }

                    return manifest;
                }
            }
            catch (Exception e)
            {
                errors.Add(e.Message);
            }

            return null;
        }
    }
}
