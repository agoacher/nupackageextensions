﻿namespace Nu.Commands
{
    using Microsoft.VisualStudio.Shell;
    using Nu.Config;
    using System;
    using System.ComponentModel.Design;
    using System.Threading.Tasks;

    public abstract class CommandBase<T> where T : CommandBase<T>, new()
    {
        private readonly CommandID commandId;

        private AsyncPackage package;
        private EnvDTE.DTE dte;

        protected CommandBase(CommandID commandId)
        {
            this.commandId = commandId;
        }

        public ISettingsProvider Settings { get; private set; }
        public IWriteToOutput OutputWriter { get; private set; }

        public async Task<T> InitializeAsync(AsyncPackage package, EnvDTE.DTE dte, ISettingsProvider settings, IWriteToOutput outputWriter)
        {
            this.package = package;
            this.dte = dte;
            Settings = settings;
            OutputWriter = outputWriter;

            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync(package.DisposalToken);

            if (!(await package.GetServiceAsync(typeof(IMenuCommandService)) is OleMenuCommandService commandService))
            {
                throw new NullReferenceException(nameof(commandService));
            }

            var menuItemCommand = new MenuCommand((s, e) => Execute(package, dte), commandId);
            commandService.AddCommand(menuItemCommand);

            return await OnInitializeAsync(package);
        }

        protected abstract Task<T> OnInitializeAsync(AsyncPackage package);

        public abstract void Execute(Package package, EnvDTE.DTE dte);
    }
}
