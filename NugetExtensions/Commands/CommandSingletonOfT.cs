﻿namespace Nu.Commands
{
    using Microsoft.VisualStudio.Shell;
    using Nu.Config;
    using Task = System.Threading.Tasks.Task;

    public static class CommandSingleton<T> where T : CommandBase<T>, new()
    {
        public static T Instance { get; set; }

        public static async Task InitializeAsync(AsyncPackage package, ISettingsProvider settings, IWriteToOutput outputWriter)
        {
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync(package.DisposalToken);
            var dte = (EnvDTE.DTE)(await package.GetServiceAsync(typeof(EnvDTE.DTE)));
            Instance = new T();
            await Instance.InitializeAsync(package, dte, settings, outputWriter);
        }
    }
}
