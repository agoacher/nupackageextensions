﻿namespace Nu
{
    using Microsoft.VisualStudio.Shell;
    using Microsoft.VisualStudio.Shell.Interop;
    using Nu.Commands;
    using Nu.Config;
    using Nu.Pack;
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Runtime.InteropServices;
    using System.Threading;
    using Task = System.Threading.Tasks.Task;

    /// <summary>
    /// This is the class that implements the package exposed by this assembly.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The minimum requirement for a class to be considered a valid package for Visual Studio
    /// is to implement the IVsPackage interface and register itself with the shell.
    /// This package uses the helper classes defined inside the Managed Package Framework (MPF)
    /// to do it: it derives from the Package class that provides the implementation of the
    /// IVsPackage interface and uses the registration attributes defined in the framework to
    /// register itself and its components with the shell. These attributes tell the pkgdef creation
    /// utility what data to put into .pkgdef file.
    /// </para>
    /// <para>
    /// To get loaded into VS, the package must be referred by &lt;Asset Type="Microsoft.VisualStudio.VsPackage" ...&gt; in .vsixmanifest file.
    /// </para>
    /// </remarks>
    [PackageRegistration(UseManagedResourcesOnly = true, AllowsBackgroundLoading = true)]
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)] // Info on this package for Help/About
    [ProvideMenuResource("Menus.ctmenu", 1)]
    [Guid(NuCommandPackage.PackageGuidString)]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "pkgdef, VS and vsixmanifest are valid VS terms")]
    public sealed class NuCommandPackage : AsyncPackage, IWriteToOutput
    {
        private IVsOutputWindowPane outputPane;
        private const string outputWindow = "Nu Output:";

        /// <summary>
        /// NuCommandPackage GUID string.
        /// </summary>
        public const string PackageGuidString = "70f5fc1e-fa3e-4e6b-ad71-10f204a55c50";

        /// <summary>
        /// Initializes a new instance of the <see cref="NuCommandPackage"/> class.
        /// </summary>
        public NuCommandPackage()
        {
            // Inside this method you can place any initialization code that does not require
            // any Visual Studio service because at this point the package object is created but
            // not sited yet inside Visual Studio environment. The place to do all the other
            // initialization is the Initialize method.
        }


        public void Write(string output)
        {
            outputPane.OutputString(output);
        }

        public void WriteLine(string output)
        {
            outputPane.OutputString($"{output}\r\n");
        }

        #region Package Members

        /// <summary>
        /// Initialization of the package; this method is called right after the package is sited, so this is the place
        /// where you can put all the initialization code that rely on services provided by VisualStudio.
        /// </summary>
        /// <param name="cancellationToken">A cancellation token to monitor for initialization cancellation, which can occur when VS is shutting down.</param>
        /// <param name="progress">A provider for progress updates.</param>
        /// <returns>A task representing the async work of package initialization, or an already completed task if there is none. Do not return null from this method.</returns>
        protected override async Task InitializeAsync(CancellationToken cancellationToken, IProgress<ServiceProgressData> progress)
        {
            // When initialized asynchronously, the current thread may be a background thread at this point.
            // Do any initialization that requires the UI thread after switching to the UI thread.
            await this.JoinableTaskFactory.SwitchToMainThreadAsync(cancellationToken);
            var settings = new IniFileSettingsProvider();

            SetWindowPane();


            await CommandSingleton<PackSolutionCommand>.InitializeAsync(this,settings, this);
        }

        private void SetWindowPane()
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            IVsOutputWindowPane pane = null;
            var outWindow = GetGlobalService(typeof(SVsOutputWindow)) as IVsOutputWindow;
            Guid generalPaneGuid = Microsoft.VisualStudio.VSConstants.GUID_OutWindowGeneralPane; // P.S. There's also the GUID_OutWindowDebugPane available.

            outWindow.GetPane(ref generalPaneGuid, out pane);
            if (pane == null)
            {
                outWindow.CreatePane(ref generalPaneGuid, outputWindow, 1, 1);
                outWindow.GetPane(ref generalPaneGuid, out pane);
                this.outputPane = pane;
            }
        }

        #endregion
    }
}
