﻿namespace Nu.Config
{
    using System;

    public interface ISettingsProvider
    {
        Version GetDefaultVersion();

        string TargetDirectory { get; }

        bool GenerateSymbols { get; }
    }
}
