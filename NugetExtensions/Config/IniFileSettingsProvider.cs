﻿namespace Nu.Config
{
    using IniParser;
    using System;
    using System.IO;

    public sealed class IniFileSettingsProvider : ISettingsProvider
    {
        private static readonly string configFileLocation =
            Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                "Nu", "nu.ini");

        private static readonly Version defaultVersion = new Version(99, 0, 0);

        private Version version;
        private bool watchForChanges;

        public IniFileSettingsProvider()
        {
            version = defaultVersion;
            watchForChanges = false;
            GenerateSymbols = true;
            TargetDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
            "Nu", "Builds");
            CheckForConfig();
        }

        public string TargetDirectory { get; private set; }

        public bool GenerateSymbols { get; set; }

        public Version GetDefaultVersion()
        {
            return version;
        }

        private void CheckForConfig()
        {
            if (!File.Exists(configFileLocation))
            {
                SaveConfig();
            }
            else
            {
                LoadConfig();
            }
        }

        private void LoadConfig()
        {
            var parser = new FileIniDataParser();
            var data = parser.ReadFile(configFileLocation);

            var configSection = data["Config"];
            if (configSection != null)
            {
                this.watchForChanges = bool.Parse(configSection["WatchForChanges"]);
            }

            var buildSection = data["Build"];
            if (buildSection != null)
            {
                var versionString = buildSection["VersionTemplate"];
                var symbolString = buildSection["GenerateSymbols"];
                var targetDirectoryString = buildSection["TargetDirectory"];

                if (!string.IsNullOrEmpty(versionString))
                {
                    if (Version.TryParse(versionString, out Version v))
                    {
                        version = v;
                    }
                }

                if (!string.IsNullOrEmpty(symbolString))
                {
                    if (bool.TryParse(symbolString, out bool r))
                    {
                        GenerateSymbols = r;
                    }
                }

                if (!string.IsNullOrEmpty(targetDirectoryString))
                {
                    try
                    {
                        var t = Path.GetFullPath(targetDirectoryString);
                        if (!Directory.Exists(t))
                        {
                            Directory.CreateDirectory(t);
                        }
                        TargetDirectory = t;
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
            }
        }

        private void SaveConfig()
        {
            var directory = Path.GetDirectoryName(configFileLocation);
            Directory.CreateDirectory(directory);
            var parser = new FileIniDataParser();
            var data = new IniParser.Model.IniData();

            var configSection = data["Config"];
            configSection["WatchForChanges"] = watchForChanges.ToString();

            var buildSection = data["Build"];
            buildSection["VersionTemplate"] = version.ToString();
            buildSection["GenerateSymbols"] = GenerateSymbols.ToString();
            buildSection["TargetDirectory"] = TargetDirectory;

            parser.WriteFile(configFileLocation, data);
        }
    }
}
