﻿namespace Nu.Config
{
    using System;

    public static class CommandConstants
    {
        /// <summary>
        /// Command menu group (command set GUID).
        /// </summary>
        public static readonly Guid CommandSet = new Guid("0c66194d-94e8-4b9a-a909-9082c7f3f75a");

        public const int PackageLocalNugetCommandId = 0x0100;
        public const int VerifyNugetCommandId = 0x0200;
    }
}
