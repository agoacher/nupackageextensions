﻿namespace Nu.Config
{
    using System;

    internal sealed class InDevelopmentSettingsProvider : ISettingsProvider
    {
        private const string targetDir = @"c:\dev\nuget";

        public string TargetDirectory => targetDir;

        public bool GenerateSymbols => true;

        private static readonly Version defaultVersion = new Version(99, 0, 0);

        public Version GetDefaultVersion()
        {
            return defaultVersion;
        }
    }
}
