﻿namespace Nu
{
    public interface IWriteToOutput
    {
        void Write(string output = "");
        void WriteLine(string output = "");
    }
}
